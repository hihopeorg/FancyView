package site.gemus.openingstartanimation;

import ohos.aafwk.ability.Ability;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.StackLayout;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.render.Canvas;
import ohos.agp.render.render3d.ViewHolder;
import ohos.agp.utils.Color;
import ohos.agp.window.dialog.CommonDialog;
import ohos.app.Context;
import ohos.bundle.IBundleManager;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.eventhandler.InnerEvent;
import ohos.global.resource.NotExistException;
import ohos.global.resource.Resource;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;

import java.io.IOException;
import java.lang.ref.SoftReference;
import java.lang.reflect.Field;
import java.util.Timer;
import java.util.TimerTask;

/**
 * @author Jackdow
 * @version 1.0
 * FancyView
 */

public class OpeningStartAnimation extends Component implements Component.DrawTask {

    private long animationInterval = 1500;
    private long animationFinishTime = 350;
    private WidthAndHeightOfView mWidthAndHeightOfView;
    private int colorOfBackground = Color.WHITE.getValue();
    private float fraction;
    private PixelMap mDrawable = null;
    private int colorOfAppIcon = Color.getIntColor("#00897b");
    private String appName = "";
    private int colorOfAppName = Color.getIntColor("#00897b");
    private String appStatement = "";
    private int colorOfAppStatement = Color.getIntColor("#607D8B");
    private DelegateRecycleView mDelegateRecycleView;
    private DrawStrategy mDrawStrategy = new NormalDrawStrategy();
    private static final int FINISHANIMATION = 1;

    private OpeningStartAnimation(Context context) {
        super(context);
        try {
            String iconName = context.getApplicationInfo().getIcon().split(":")[1];
            int iconId = getDrawableResourceId(context, "Media_" + iconName);
            mDrawable = convert(context, iconId);

            String name = context.getApplicationInfo().getLabel().split(":")[1];
            appName = context.getResourceManager().getElement(getDrawableResourceId(context, "String_" + name)).getString();
            appStatement = "Sample Statement";

        } catch (Exception e) {
            e.printStackTrace();
        }

        addDrawTask(this);
    }

    private PixelMap convert(Context context, int resId) {
        PixelMap pixelMap = null;
        try {
            Resource bgResource = context.getResourceManager().getResource(resId);
            ImageSource.SourceOptions srcOpts = new ImageSource.SourceOptions();
            srcOpts.formatHint = "image/png";
            ImageSource imageSource = ImageSource.create(bgResource, srcOpts);
            ImageSource.DecodingOptions decodingOptions = new ImageSource.DecodingOptions();
            pixelMap = imageSource.createPixelmap(decodingOptions);
        } catch (IOException | NotExistException e) {
            e.printStackTrace();
        }
        return pixelMap;
    }

    private int getDrawableResourceId(Context context, String name) {
        try {
            Class resourceTableClz = Class.forName(context.getAbilityPackageContext().getBundleName() + ".ResourceTable");
            Field field = resourceTableClz.getField(name);
            int id = field.getInt(null);
            return id;
        } catch (ClassNotFoundException | NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return -1;
    }

    /**
     * 设置完成的百分比
     *
     * @param fraction 百分比
     */
    @SuppressWarnings("unused")
    private void setFraction(float fraction) {
        this.fraction = fraction;
        invalidate();
    }

    {
        StackLayout.LayoutConfig layoutParams;
        layoutParams = new StackLayout.LayoutConfig(StackLayout.LayoutConfig.MATCH_PARENT,
                StackLayout.LayoutConfig.MATCH_PARENT);
        this.setLayoutConfig(layoutParams);
        mWidthAndHeightOfView = new WidthAndHeightOfView();
    }


    @Override
    public void onDraw(Component component, Canvas canvas) {
        canvas.drawColor(colorOfBackground, Canvas.PorterDuffMode.DST); //绘制背景色
        mWidthAndHeightOfView.setHeight(getHeight());
        mWidthAndHeightOfView.setWidth(getWidth());
        mDrawStrategy.drawAppIcon(canvas, fraction, mDrawable, colorOfAppIcon,
                mWidthAndHeightOfView);
        mDrawStrategy.drawAppName(canvas, fraction, appName, colorOfAppName,
                mWidthAndHeightOfView);
        mDrawStrategy.drawAppStatement(canvas, fraction, appStatement, colorOfAppStatement,
                mWidthAndHeightOfView);
    }

    public long getAnimationInterval() {
        return animationInterval;
    }

    public long getAnimationFinishTime() {
        return animationFinishTime;
    }


    public PixelMap getmDrawable() {
        return mDrawable;
    }

    public int getColorOfAppIcon() {
        return colorOfAppIcon;
    }

    public String getAppName() {
        return appName;
    }

    public int getColorOfAppName() {
        return colorOfAppName;
    }

    public String getAppStatement() {
        return appStatement;
    }

    public int getColorOfAppStatement() {
        return colorOfAppStatement;
    }

    public int getColorOfBackground() {
        return colorOfBackground;
    }

    public DrawStrategy getmDrawStrategy() {
        return mDrawStrategy;
    }


    /**
     * 显示动画
     *
     * @param mactivity 显示动画的界面
     */
    public void show(Ability mactivity) {
        SoftReference<Ability> softReference = new SoftReference<>(mactivity);
        final Ability activity = softReference.get();

        CommonDialog dialog = new CommonDialog(activity.getContext());
        dialog.setContentCustomComponent(this);
        dialog.show();

        AnimatorValue animatorValue = new AnimatorValue();
        animatorValue.setDuration(animationInterval - 50);
        animatorValue.setDelay(100);
        animatorValue.setLoopedCount(0);
        animatorValue.setCurveType(Animator.CurveType.LINEAR);
        animatorValue.setValueUpdateListener((animatorValue1, value) -> {
            setFraction(value);
        });
        animatorValue.start();

        //处理动画定时
        EventHandler handler = new EventHandler(EventRunner.getMainEventRunner()) {
            @Override
            protected void processEvent(InnerEvent event) {
                super.processEvent(event);
                if (event.eventId == FINISHANIMATION) {
                    moveAnimation(activity);
                    dialog.hide();
                }
            }
        };
        //动画定时器
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                InnerEvent message = InnerEvent.get();
                message.eventId = FINISHANIMATION;
                handler.sendEvent(message);
            }
        }, animationInterval + 400);
    }

    /**
     * 隐藏动画view
     *
     * @param activity 当前活动
     */
    private void moveAnimation(Ability activity) {
        this.createAnimatorProperty()
                .scaleX(0)
                .scaleY(0)
//                .withLayer()
                .alpha(0)
                .setDuration(animationFinishTime);

        mDelegateRecycleView.finishAnimation();
    }

    /**
     * 使用Builder模式创建对象
     */
    public static final class Builder implements DelegateRecycleView {
        OpeningStartAnimation mOpeningStartAnimation;
        public static DrawStrategy mDrawStrategy;

        public Builder(Context context) {
            mOpeningStartAnimation = new OpeningStartAnimation(context);
            mOpeningStartAnimation.mDelegateRecycleView = this;
        }

        public OpeningStartAnimation getmOpeningStartAnimation() {
            return mOpeningStartAnimation;
        }

        /**
         * 设置动画时间的方法
         *
         * @param animationInterval 动画时间
         * @return Builder对象
         */
        public Builder setAnimationInterval(long animationInterval) {
            mOpeningStartAnimation.animationInterval = animationInterval;
            return this;
        }

        /**
         * 设置动画消失的时间
         *
         * @param animationFinishTime 动画消失的时间
         * @return Builder对象
         */
        public Builder setAnimationFinishTime(long animationFinishTime) {
            mOpeningStartAnimation.animationFinishTime = animationFinishTime;
            return this;
        }

        /**
         * 设置动画图标
         *
         * @param drawable 动画图标
         * @return Builder对象
         */
        public Builder setAppIcon(PixelMap drawable) {
            mOpeningStartAnimation.mDrawable = drawable;
            return this;
        }

        /**
         * 设置图标绘制辅助颜色
         *
         * @param colorOfAppIcon 辅助颜色
         * @return Builder对象
         */
        public Builder setColorOfAppIcon(int colorOfAppIcon) {
            mOpeningStartAnimation.colorOfAppIcon = colorOfAppIcon;
            return this;
        }

        /**
         * 设置要绘制的app名称
         *
         * @param appName app名称
         * @return Builder对象
         */
        public Builder setAppName(String appName) {
            mOpeningStartAnimation.appName = appName;
            return this;
        }

        /**
         * 设置app名称的颜色
         *
         * @param colorOfAppName app名称颜色
         * @return Builder对象
         */
        public Builder setColorOfAppName(int colorOfAppName) {
            mOpeningStartAnimation.colorOfAppName = colorOfAppName;
            return this;
        }

        /**
         * 设置app一句话描述
         *
         * @param appStatement app一句话描述
         * @return Builder对象
         */
        public Builder setAppStatement(String appStatement) {
            mOpeningStartAnimation.appStatement = appStatement;
            return this;
        }

        /**
         * 设置一句话描述的字体颜色
         *
         * @param colorOfAppStatement 字体颜色
         * @return Builder对象
         */
        public Builder setColorOfAppStatement(int colorOfAppStatement) {
            mOpeningStartAnimation.colorOfAppStatement = colorOfAppStatement;
            return this;
        }

        /**
         * 设置背景颜色
         *
         * @param colorOfBackground 背景颜色对应的int值
         * @return Builder对象
         */
        public Builder setColorOfBackground(int colorOfBackground) {
            mOpeningStartAnimation.colorOfBackground = colorOfBackground;
            return this;
        }

        /**
         * 开放绘制策略接口，可由用户自行定义
         *
         * @param drawStrategy 绘制接口
         * @return Builder对象
         */
        public Builder setDrawStategy(DrawStrategy drawStrategy) {
            mOpeningStartAnimation.mDrawStrategy = drawStrategy;
            this.mDrawStrategy = drawStrategy;
            return this;
        }

        /**
         * 创建开屏动画
         *
         * @return 创建出的开屏动画
         */
        public OpeningStartAnimation create() {
            return mOpeningStartAnimation;
        }

        @Override
        public void finishAnimation() {
            mOpeningStartAnimation = null;
        }
    }
}
