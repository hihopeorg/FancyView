package site.gemus.openingstartanimation;


import ohos.agp.components.element.PixelMapElement;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.media.image.PixelMap;

/**
 * @author Jackdow
 * @version 1.0
 *          FancyView
 */

class RYBDrawStrategyStateOne implements RYBDrawStrategyStateInterface {
    @Override
    public void drawIcon(Canvas canvas, float fraction, PixelMap drawable, int colorOfIcon,
                         WidthAndHeightOfView widthAndHeightOfView) {
        float newFraction = fraction / 0.65f;
        int centerX = widthAndHeightOfView.getWidth() / 2;
        int centerY = widthAndHeightOfView.getHeight() / 2 - 150;
        Paint paint = new Paint();
        canvas.save();
        paint.setColor(new Color(Color.getIntColor("#e53935")));
        if (newFraction <= 0.33f)  {
            canvas.drawCircle(centerX, centerY - 50, 100 * (newFraction / 0.33f), paint);
        } else {
            canvas.drawCircle(centerX, centerY - 50, 100, paint);
        }
        if (newFraction > 0.33f) {
            paint.setColor(new Color(Color.getIntColor("#fdd835")));
            if (newFraction <= 0.66f)
                canvas.drawCircle(centerX -35, centerY + 35,100 * ((newFraction - 0.33f) / 0.33f), paint);
            else
                canvas.drawCircle(centerX -35, centerY + 35,100, paint);
        }
        if (newFraction > 0.66f) {
            paint.setColor(new Color(Color.getIntColor("#1e88e5")));
            if (newFraction <= 1f)
                canvas.drawCircle(centerX + 35, centerY + 35, 100 * ((newFraction - 0.66f) / 0.34f), paint);
            else
                canvas.drawCircle(centerX + 35, centerY + 35, 100, paint);
        }
        canvas.restore();
    }
}
