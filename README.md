﻿# FancyView

**本项目是基于开源项目FancyView进行ohos化的移植和开发的，可以通过项目标签以及github地址（ https://github.com/wongzy/FancyView ）追踪到原项目版本**

#### 项目介绍

- 项目名称：FancyView
- 所属系列：ohos的第三方组件适配移植
- 功能：开屏动画
- 项目移植状态：完成
- 调用差异：无
- 项目作者和维护人：hihope
- 联系方式：hihope@hoperun.com
- 原项目Doc地址：https://github.com/wongzy/FancyView
- 原项目基线版本：无release版本
- 编程语言：Java
- 外部库依赖：无

#### 效果展示：
![avatar](animation.gif)

#### 安装教程

##### 方案一：

1. 编译依赖库 fancyview.har。
2. 启动 DevEco Studio，将编译的har包，导入工程目录“entry->libs”下。
3. 在moudle级别下的build.gradle文件中添加依赖，在dependences标签中增加对libs目录下har包的引用。
```
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
	……
}
```
 4.在导入的har包上点击右键，选择“Add as Library”对包进行引用，选择需要引用的模块，并点击“OK”即引用成功。

##### 方案二：
  1. 在工程的build.gradle的allprojects中，添加HAR所在的Maven仓地址:
```
 repositories {
     maven {
         url 'http://106.15.92.248:8081/repository/Releases/' 
     }
 }
```
  2. 在应用模块的build.gradle的dependencies闭包中，添加如下代码:
```
 dependencies {
     implementation 'site.gemus.openingstartanimation:fancyview:1.0.0'
 }
```

#### 使用说明
 1、使用方法
```java
 OpeningStartAnimation openingStartAnimation = new OpeningStartAnimation.Builder(this)
    .setDrawStategy(new NormalDrawStrategy()) //设置动画效果
    .create();
openingStartAnimation.show(this);
```
 2、除此之外，还可以设置文字、图标、一句话描述、动画时间等等，也可以自定义开屏动画，开放了策略接口，像这样。
```java
OpeningStartAnimation openingStartAnimation = new OpeningStartAnimation.Builder(this)
                .setDrawStategy(new DrawStrategy() {
                    @Override
                    public void drawAppName(Canvas canvas, float fraction, String name, int colorOfAppName, WidthAndHeightOfView widthAndHeightOfView) {
                        
                    }

                    @Override
                    public void drawAppIcon(Canvas canvas, float fraction, Drawable icon, int colorOfIcon, WidthAndHeightOfView widthAndHeightOfView) {

                    }

                    @Override
                    public void drawAppStatement(Canvas canvas, float fraction, String statement, int colorOfStatement, WidthAndHeightOfView widthAndHeightOfView) {

                    }
                })
                .create();
```

#### 版本迭代

- v1.0.0 
- 实现以下功能
	1. 实现四种基本的开屏动画

#### 版权和许可信息
- Apache Licence
