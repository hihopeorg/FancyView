package site.gemus.fancyview;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import site.gemus.openingstartanimation.*;

public class MainAbility extends Ability implements Component.ClickedListener {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        Button button1, button2, button3, button4;
        button1 = (Button) findComponentById(ResourceTable.Id_button_normal);
        button2 = (Button) findComponentById(ResourceTable.Id_button_ryb);
        button3 = (Button) findComponentById(ResourceTable.Id_button_line);
        button4 = (Button) findComponentById(ResourceTable.Id_button_rotation);

        button1.setClickedListener(this);
        button2.setClickedListener(this);
        button3.setClickedListener(this);
        button4.setClickedListener(this);
    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_button_normal:
                OpeningStartAnimation openingStartAnimation = new OpeningStartAnimation.Builder(this)
                        .setDrawStategy(new NormalDrawStrategy())
                        .create();
                openingStartAnimation.show(this);
                break;
            case ResourceTable.Id_button_ryb:
                OpeningStartAnimation openingStartAnimation1 = new OpeningStartAnimation.Builder(this)
                        .setDrawStategy(new RedYellowBlueDrawStrategy())
                        .create();
                openingStartAnimation1.show(this);
                break;
            case ResourceTable.Id_button_line:
                OpeningStartAnimation openingStartAnimation2 = new OpeningStartAnimation.Builder(this)
                        .setDrawStategy(new LineDrawStrategy())
                        .create();
                openingStartAnimation2.show(this);
                break;
            case ResourceTable.Id_button_rotation:
                OpeningStartAnimation openingStartAnimation3 = new OpeningStartAnimation.Builder(this)
                        .setDrawStategy(new RotationDrawStrategy())
                        .create();
                openingStartAnimation3.show(this);
                break;
            default:
                break;
        }
    }
}
