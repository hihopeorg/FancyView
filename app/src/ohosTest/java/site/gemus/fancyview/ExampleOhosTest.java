package site.gemus.fancyview;

import ohos.aafwk.ability.Ability;
import ohos.agp.components.Button;
import ohos.agp.window.dialog.CommonDialog;
import org.junit.After;
import org.junit.Test;
import site.gemus.openingstartanimation.*;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class ExampleOhosTest {

    @After
    public void after() {
        sleep();
        EventHelper.clearAbilities();
    }

    @Test
    public void oneAnimation() {
        Ability ability = EventHelper.startAbility(MainAbility.class);
        assertNotNull(ability);
        sleep();
        Button button1 = (Button) ability.findComponentById(ResourceTable.Id_button_normal);
        EventHelper.triggerClickEvent(ability, button1);
        sleep();
        DrawStrategy strategy = OpeningStartAnimation.Builder.mDrawStrategy;
        assertTrue(strategy instanceof NormalDrawStrategy);
    }

    @Test
    public void twoAnimation() {
        Ability ability = EventHelper.startAbility(MainAbility.class);
        assertNotNull(ability);
        sleep();
        Button button1 = (Button) ability.findComponentById(ResourceTable.Id_button_ryb);
        EventHelper.triggerClickEvent(ability, button1);
        sleep();
        DrawStrategy strategy = OpeningStartAnimation.Builder.mDrawStrategy;
        assertTrue(strategy instanceof RedYellowBlueDrawStrategy);
    }

    @Test
    public void threeAnimation() {
        Ability ability = EventHelper.startAbility(MainAbility.class);
        assertNotNull(ability);
        sleep();
        Button button1 = (Button) ability.findComponentById(ResourceTable.Id_button_line);
        EventHelper.triggerClickEvent(ability, button1);
        sleep();
        DrawStrategy strategy = OpeningStartAnimation.Builder.mDrawStrategy;
        assertTrue(strategy instanceof LineDrawStrategy);
    }

    @Test
    public void fourAnimation() {
        Ability ability = EventHelper.startAbility(MainAbility.class);
        assertNotNull(ability);
        sleep();
        Button button1 = (Button) ability.findComponentById(ResourceTable.Id_button_rotation);
        EventHelper.triggerClickEvent(ability, button1);
        sleep();
        DrawStrategy strategy = OpeningStartAnimation.Builder.mDrawStrategy;
        assertTrue(strategy instanceof RotationDrawStrategy);
    }


    private void sleep() {
        try {
            Thread.sleep(1500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}