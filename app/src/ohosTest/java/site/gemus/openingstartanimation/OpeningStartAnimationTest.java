package site.gemus.openingstartanimation;

import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.global.resource.Resource;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import org.junit.Test;
import site.gemus.fancyview.ResourceTable;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class OpeningStartAnimationTest {

    @Test
    public void setAnimationInterval() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        OpeningStartAnimation openingStartAnimation = new OpeningStartAnimation.Builder(context)
                .setAnimationInterval(1000)
                .create();
        assertEquals(openingStartAnimation.getAnimationInterval(), 1000);
    }

    @Test
    public void setAnimationFinishTime() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        OpeningStartAnimation openingStartAnimation = new OpeningStartAnimation.Builder(context)
                .setAnimationFinishTime(1000)
                .create();
        assertEquals(openingStartAnimation.getAnimationFinishTime(), 1000);
    }

    @Test
    public void setAppIcon() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        PixelMap pixelMap = convert(context, ResourceTable.Media_icon);
        OpeningStartAnimation openingStartAnimation = new OpeningStartAnimation.Builder(context)
                .setAppIcon(pixelMap)
                .create();
        assertTrue(openingStartAnimation.getmDrawable().isSameImage(pixelMap));
    }

    @Test
    public void setColorOfAppIcon() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        OpeningStartAnimation openingStartAnimation = new OpeningStartAnimation.Builder(context)
                .setColorOfAppIcon(1000)
                .create();
        assertEquals(openingStartAnimation.getColorOfAppIcon(), 1000);
    }

    @Test
    public void setAppName() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        OpeningStartAnimation openingStartAnimation = new OpeningStartAnimation.Builder(context)
                .setAppName("hello")
                .create();
        assertEquals(openingStartAnimation.getAppName(), "hello");
    }

    @Test
    public void setColorOfAppName() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        OpeningStartAnimation openingStartAnimation = new OpeningStartAnimation.Builder(context)
                .setColorOfAppName(999)
                .create();
        assertEquals(openingStartAnimation.getColorOfAppName(), 999);
    }

    @Test
    public void setAppStatement() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        OpeningStartAnimation openingStartAnimation = new OpeningStartAnimation.Builder(context)
                .setAppStatement("999")
                .create();
        assertEquals(openingStartAnimation.getAppStatement(), "999");
    }

    @Test
    public void setColorOfAppStatement() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        OpeningStartAnimation openingStartAnimation = new OpeningStartAnimation.Builder(context)
                .setColorOfAppStatement(111)
                .create();
        assertEquals(openingStartAnimation.getColorOfAppStatement(), 111);
    }

    @Test
    public void setColorOfBackground() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        OpeningStartAnimation openingStartAnimation = new OpeningStartAnimation.Builder(context)
                .setColorOfBackground(111)
                .create();
        assertEquals(openingStartAnimation.getColorOfBackground(), 111);
    }


    private PixelMap convert(Context context, int resId) {
        PixelMap pixelMap = null;
        try {
            Resource bgResource = context.getResourceManager().getResource(resId);
            ImageSource.SourceOptions srcOpts = new ImageSource.SourceOptions();
            srcOpts.formatHint = "image/png";
            ImageSource imageSource = ImageSource.create(bgResource, srcOpts);
            ImageSource.DecodingOptions decodingOptions = new ImageSource.DecodingOptions();
            pixelMap = imageSource.createPixelmap(decodingOptions);
        } catch (IOException | NotExistException e) {
            e.printStackTrace();
        }
        return pixelMap;
    }

}
